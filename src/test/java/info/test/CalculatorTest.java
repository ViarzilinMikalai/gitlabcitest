package info.test;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    Calculator calculator = new Calculator();

    @Test
    public void sum() {
        int actual = 19;
        assertEquals(calculator.sum(9, 10), actual);

    }
}